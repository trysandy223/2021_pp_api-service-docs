# PPIT MANTAPP API SERVICE

PP Financial Dashboard API Services adalah layanan web services berbasis REST APIs yang ditujukan untuk manajemen data financial di PT PP (Persero) Tbk. Layanan ini dibangun menggunakan bahasa pemrograman Golang (Go) yang berfokus ke performa dan kecepatan. Untuk melihat dokumentasinya silakan akses https://readthedocs.org/
