Persyaratan
============

Persyaratan atau requirement yang dibutuhkan untuk menjalankan PP Financial Dashboard API Services antara lain:

* Golang (Go) >= 1.16
* MySQL >= 8.0