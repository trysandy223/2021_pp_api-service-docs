Struktur Projek
===============

Struktur projek dari PP Financial Dashboard API Services ini mengacu pada standar clean architecture pengembangan menggunakan bahasa pemrograman Golang (Go).

.. code-block::

    ├── config
    │   ├── c_mysql
    │   │   └── c_mysql.go
    │   └── config.go
    ├── ...
    ├── src
    │   ├── modules
    │   │   ├── ...
    │   │   └── auth
    │   │       ├── delivery
    │   │       │   └── auth_delivery.go
    │   │       ├── domain
    │   │       │   └── auth.go
    │   │       ├── repository
    │   │       │   └── auth_repositor.go
    │   │       └── usecase
    │   │           └── auth_usecase.go
    │   ├── helpers
    │   │   ├── ...
    │   │   └── auth.go
    │   └── router
    │       └── router.go
    ├── .env
    ├── .gitignore
    ├── env.example
    ├── go.mod
    ├── go.sum
    ├── main.go
    └── Readme.md