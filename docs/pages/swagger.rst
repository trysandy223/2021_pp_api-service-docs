Menjalankan Swagger
===================

PP Financial Dashboard API Services dapat diujicoba dan dijalankan dengan menggunakan Swagger.
Swagger sendiri merupakan Bahasa Deskripsi Antarmuka untuk mendeskripsikan RESTful APIs yang diekspresikan menggunakan JSON.

Konfigurasi
-----------

1. Buka browser, lalu arahkan ke alamat ``http://localhost:9000/doc-api/index.html``. Atau jika berada di server, arahkan ke alamat sesuai dengan alamat URL server. Contoh: ``https://mantapp-api-qa.ptpp.co.id/doc-api/index.html``.

2. Pada bagian **Schemes**, sesuaikan protokol dengan yang berjalan pada server.

.. image:: ../_static/swagger/config-scheme.png
  :width: 400
  :align: center

3. Klik tombol **Authorize** yang berada di pojok kanan, lalu masukkan nilai **KEY_API_KEY** yang sebelumnya sudah diset di file ``.env``. Setelah itu klik kembali tombol **Authorize**.

.. image:: ../_static/swagger/config-api-key.png
  :width: 400
  :align: center

Login
-----

Setelah langkah konfigurasi dijalankan, saatnya untuk melakukan proses login.

1. Buka endpoint Login, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi payload ``email`` dan ``password`` sesuai dengan kredensial yang Anda miliki. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "email": "admin@ptpp.com",
        "password": "rhyr56!(5gts451"
    }


2. Setelah login berhasil, maka di bagian bawah akan muncul ``response`` yang berisi token berupa random string. Token inilah yang nantinya digunakan untuk menjalankan API lainnya. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": {
            "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBfbmFtZSI6IkFQSV9QUF9GSW5hbmNpYWxfRGFzYm9hcmQgUUEiLCJuYW1lX2lkIjoxLCJuYW1lIjoiQWRtaW4iLCJyb2xlIjoiU3VwZXJhZG1pbiIsInN1Yl9wYXJlbnRfY29tcGFueV9pZCI6MSwiY29tcGFueV9pZCI6WzEsMiwzLDQsOSwxMiwxNSwxOCwyMSwyNCwyNywzMCwzMywzNiwzOSw0Miw0NSw0Niw0Nyw0OCw1MCw1MSw1Myw1NCw1NSw1Niw1Nyw1OCw1OSw2MCw2MSw2Miw2Myw2NCw2NSw2Niw2Nyw2OCw2OSw3MCw3MSw3Ml0sImV4cCI6MTYzODc4MDY5OCwiaXNzIjoiZGV2IEdhbWF0ZWNobm8iLCJzdWIiOiJhZG1pbkBhZG1pbi5jb20ifQ.JUHC6Gb9X2bq0ZpJhsTBEIgU-6anO1JTv9pHLesQkIY"
        }
    }

.. warning::
    Token yang dihasilkan dari proses login bersifat rahasia. Jangan membagikannya kepada siapapun.
    
    Token hanya berlaku selama tiga jam. Lebih dari itu Anda perlu login ulang untuk meng-generate token baru.

API References
--------------

Bagian ini menjelaskan bagaimana cara menjalankan API References seperti Account Group, Bank, Category, dan lain-lain melalui Swagger. Endpoint yang tersedia di API References ada dua, yakni **Get** dan **Get By Id**.

Get
^^^

1. Buka endpoint Get di salah satu API References, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

2. Isi parameter **display** dengan ``all`` untuk menampilkan semua data, atau ``page`` untuk menampilkan beberapa data.

3. Jika Anda memilih ``page``, Anda perlu mengisi parameter **limit** dengan angka sesuai dengan data yang ingin ditampilkan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-references.png
  :width: 600
  :align: center

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data References. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": [
            {
                "id": 1,
                "code": "002",
                "name": "PT BANK RAKYAT INDONESIA (PERSERO) Tbk",
                "sorting": 1,
                "status": 1,
                "created_at": "2021-06-10T02:01:04+07:00",
                "updated_at": "2021-06-10T02:01:04+07:00"
            },
            {
                "id": 2,
                "code": "008",
                "name": "PT BANK MANDIRI (PERSERO) Tbk",
                "sorting": 2,
                "status": 1,
                "created_at": "2021-06-10T02:01:04+07:00",
                "updated_at": "2021-06-10T02:01:04+07:00"
            }
        ]
    }

Get By Id
^^^^^^^^^

1. Buka endpoint Get By Id di salah satu API References, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data References yang ingin ditampilkan.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-by-id-references.png
  :width: 600
  :align: center

3. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data References. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": {
            "id": 1,
            "code": "002",
            "name": "PT BANK RAKYAT INDONESIA (PERSERO) Tbk",
            "sorting": 1,
            "status": 1,
            "created_at": "2021-06-10T02:01:04+07:00",
            "updated_at": "2021-06-10T02:01:04+07:00"
        }
    }

API Master Project
------------------

Bagian ini menjelaskan bagaimana cara menjalankan API Master Project melalui Swagger. Endpoint yang tersedia di API Master Project ada empat, yakni **Get**, **Post**, **Get By Id**, dan **Put**.

Get
^^^

1. Buka endpoint Get di API Master Project, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

2. Isi parameter **display** dengan ``all`` untuk menampilkan semua data, atau ``page`` untuk menampilkan beberapa data.

3. Jika Anda memilih ``page``, Anda perlu mengisi parameter **limit** dengan angka sesuai dengan data yang ingin ditampilkan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-references.png
  :width: 600
  :align: center

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Master Project. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": [
            {
                "id": 1522,
                "unit_id": 0,
                "company": {
                    "id": 15,
                    "name": "PP Presisi"
                },
                "code": "string",
                "name": "string",
                "manager": "string",
                "start_date": "2021-01-01T00:00:00+07:00",
                "finish_date": "2021-12-31T00:00:00+07:00",
                "rab": 0,
                "rkn": 0,
                "province": "string",
                "city": "string",
                "project_type_id": 0,
                "project_type_name": "",
                "project_strategic_id": 0,
                "project_strategic_name": "",
                "payment_type_id": 0,
                "payment_type_name": "",
                "business_segment_id": 0,
                "business_segment_name": "",
                "customer_id": 0,
                "customer_name": "",
                "created_at": "2021-12-07T11:02:54+07:00",
                "updated_at": "2021-12-07T11:02:54+07:00"
            },
            {
                "id": 1521,
                "unit_id": 0,
                "company": {
                    "id": 15,
                    "name": "PP Presisi"
                },
                "code": "string",
                "name": "string",
                "manager": "string",
                "start_date": "2021-01-01T00:00:00+07:00",
                "finish_date": "2021-12-31T00:00:00+07:00",
                "rab": 0,
                "rkn": 0,
                "province": "string",
                "city": "string",
                "project_type_id": 0,
                "project_type_name": "",
                "project_strategic_id": 0,
                "project_strategic_name": "",
                "payment_type_id": 0,
                "payment_type_name": "",
                "business_segment_id": 0,
                "business_segment_name": "",
                "customer_id": 0,
                "customer_name": "",
                "created_at": "2021-12-07T11:01:38+07:00",
                "updated_at": "2021-12-07T11:01:38+07:00"
            }
        ]
    }

Post
^^^^

1. Buka endpoint Post di API Master Project, lalu klik tombol **Try it out** yang berada di pojok kanan atas.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Master Project yang ingin disimpan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "company_id": 1,
        "unit_id": 1,
        "code": "1034",
        "name": "Project PT Gamatechno",
        "manager": "Trisandy",
        "start_date": "2021-12-01",
        "finish_date": "2022-12-01",
        "rab": 1000000000,
        "rkn": 500000000,
        "province": "D.I. Yogyakarta",
        "city": "Yogyakarta",
        "project_type_id": 1,
        "project_strategic_id": 1,
        "payment_type_id": 1,
        "business_segment_id": 1,
        "customer_id": 1
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

Get By Id
^^^^^^^^^

1. Buka endpoint Get By Id di API Master Project, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Master Project yang ingin ditampilkan.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-by-id-project.png
  :width: 600
  :align: center

3. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Master Project. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": {
            "id": 1,
            "unit_id": 4,
            "company": {
                "id": 45,
                "name": "PT Griyaton Indonesia"
            },
            "code": "12testing",
            "name": "(Testing)Perbaikan Jalan Desa pp Edit",
            "manager": "NURI EKOSI",
            "start_date": "2016-09-05",
            "finish_date": "2021-10-05",
            "rab": 10000,
            "rkn": 20000,
            "province": "BALI",
            "city": "DENPASAR",
            "project_type_id": 2,
            "project_type_name": "INVESTMENT",
            "project_strategic_id": 2,
            "project_strategic_name": "NON PSN",
            "payment_type_id": 3,
            "payment_type_name": "Percentage per Progress",
            "business_segment_id": 2,
            "business_segment_name": "PROPERTY",
            "customer_id": 1,
            "customer_name": "Testing data",
            "created_at": "2021-10-06T02:01:00+07:00",
            "updated_at": "2021-11-29T15:24:26+07:00"
        }
    }

Put
^^^

1. Buka endpoint Put di API Master Project, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Master Project yang ingin diubah datanya.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Master Project yang ingin diubah. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "company_id": 2,
        "unit_id": 2,
        "code": "1035",
        "name": "Project PT Gamatechno Indonesia",
        "manager": "Andry Mahany",
        "start_date": "2021-12-02",
        "finish_date": "2022-12-02",
        "rab": 3000000000,
        "rkn": 700000000,
        "province": "D.I. Yogyakarta",
        "city": "Yogyakarta",
        "project_type_id": 2,
        "project_strategic_id": 2,
        "payment_type_id": 2,
        "business_segment_id": 2,
        "customer_id": 2
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

API Master Customer
-------------------

Bagian ini menjelaskan bagaimana cara menjalankan API Master Customer melalui Swagger. Endpoint yang tersedia di API Master Customer ada empat, yakni **Get**, **Post**, **Get By Id**, dan **Put**.

Get
^^^

1. Buka endpoint Get di API Master Customer, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

2. Isi parameter **display** dengan ``all`` untuk menampilkan semua data, atau ``page`` untuk menampilkan beberapa data.

3. Jika Anda memilih ``page``, Anda perlu mengisi parameter **limit** dengan angka sesuai dengan data yang ingin ditampilkan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-references.png
  :width: 600
  :align: center

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Master Customer. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": [
            {
                "id": 954,
                "company": {
                    "id": 1,
                    "name": "PP Konsolidasi"
                },
                "title": "Customer testing Proyek",
                "code": "12testing",
                "name": "Testing data",
                "search_term": "melbourne",
                "street": "Little Conllins st",
                "city": "Melbourne",
                "region": "Victoria",
                "country": "Australia",
                "postal_code": "`12345",
                "phone": "0283747284",
                "mobile_phone": "08765432456",
                "extension_1": "09581",
                "extension_2": "09582",
                "fax": "54321",
                "email": "testing@testing.com",
                "npwp": "12334565541",
                "credit_limit": 2000000000,
                "account_group_id": 2,
                "account_group_name": "ACCOUNT GROUP 2",
                "reconsiliation_account_id": 1,
                "reconsiliation_account_name": "Piutang Konstruksi",
                "planning_group_id": 4,
                "planning_group_name": "Asing",
                "terms_of_payment_id": 3,
                "terms_of_payment_name": "within 21 days Due net",
                "house_bank_id": 10,
                "house_bank_name": "BRIIDPLB0001",
                "currency_id": 13,
                "currency_name": "Australian Dollar",
                "tax_classification_id": 1,
                "tax_classification_name": "Ekspor, ZEE, Berikat",
                "created_at": "2021-11-29T15:17:33+07:00",
                "updated_at": "2021-11-29T15:17:33+07:00"
            },
            {
                "id": 256,
                "company": {
                    "id": 2,
                    "name": "PP Induk"
                },
                "title": "",
                "code": "0130000056",
                "name": "SINAS MAS GROUP ",
                "search_term": "",
                "street": "SINARMAS SINAR MAS LAND PLAZA TOWER 2, 33/F",
                "city": "JAKARTA PUSAT",
                "region": "",
                "country": "",
                "postal_code": "",
                "phone": "",
                "mobile_phone": "",
                "extension_1": "",
                "extension_2": "",
                "fax": "",
                "email": "",
                "npwp": "",
                "credit_limit": 0,
                "account_group_id": 3,
                "account_group_name": "ZC03",
                "reconsiliation_account_id": 0,
                "reconsiliation_account_name": "",
                "planning_group_id": 0,
                "planning_group_name": "",
                "terms_of_payment_id": 0,
                "terms_of_payment_name": "",
                "house_bank_id": 0,
                "house_bank_name": "",
                "currency_id": 0,
                "currency_name": "",
                "tax_classification_id": 0,
                "tax_classification_name": "",
                "created_at": "2021-06-10T02:01:04+07:00",
                "updated_at": "2021-06-10T02:01:04+07:00"
            }
        ]
    }

Post
^^^^

1. Buka endpoint Post di API Master Customer, lalu klik tombol **Try it out** yang berada di pojok kanan atas.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Master Customer yang ingin disimpan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "company_id": 1,
        "title": "PT",
        "code": "XX001",
        "name": "HUTAMA KARYA INFRASTRUKTUR",
        "search_term": "Hutama, Karya, Infrastruktur",
        "street": "Jatinegara",
        "city": "Jakarta Timur",
        "region": "DKI Jakarta",
        "country": "Indonesia",
        "postal_code": "13340",
        "phone": "29634957",
        "mobile_phone": "-",
        "extension_1": "021",
        "extension_2": "021",
        "fax": "29634957",
        "email": "-",
        "npwp": "-",
        "credit_limit": 0,
        "account_group_id": 1,
        "reconsiliation_account_id": 1,
        "planning_group_id": 1,
        "terms_of_payment_id": 1,
        "house_bank_id": 1,
        "currency_id": 1,
        "tax_classification_id": 1
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

Get By Id
^^^^^^^^^

1. Buka endpoint Get By Id di API Master Customer, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Master Customer yang ingin ditampilkan.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-by-id-customer.png
  :width: 600
  :align: center

3. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Master Customer. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": {
            "id": 256,
            "company": {
                "id": 2,
                "name": "PP Induk"
            },
            "title": "",
            "code": "0130000056",
            "name": "SINAS MAS GROUP ",
            "search_term": "",
            "street": "SINARMAS SINAR MAS LAND PLAZA TOWER 2, 33/F",
            "city": "JAKARTA PUSAT",
            "region": "",
            "country": "",
            "postal_code": "",
            "phone": "",
            "mobile_phone": "",
            "extension_1": "",
            "extension_2": "",
            "fax": "",
            "email": "",
            "npwp": "",
            "credit_limit": 0,
            "account_group_id": 3,
            "account_group_name": "ZC03",
            "reconsiliation_account_id": 0,
            "reconsiliation_account_name": "",
            "planning_group_id": 0,
            "planning_group_name": "",
            "terms_of_payment_id": 0,
            "terms_of_payment_name": "",
            "house_bank_id": 0,
            "house_bank_name": "",
            "currency_id": 0,
            "currency_name": "",
            "tax_classification_id": 0,
            "tax_classification_name": "",
            "created_at": "2021-06-10T02:01:04+07:00",
            "updated_at": "2021-06-10T02:01:04+07:00"
        }
    }

Put
^^^

1. Buka endpoint Put di API Master Customer, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Master Customer yang ingin diubah datanya.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Master Customer yang ingin diubah. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "company_id": 1,
        "title": "PT",
        "code": "XX00123",
        "name": "HUTAMA KARYA INFRASTRUKTUR 001",
        "search_term": "Hutama, Karya, Infrastruktur, 001",
        "street": "Jatinegara",
        "city": "Jakarta Timur",
        "region": "DKI Jakarta",
        "country": "Indonesia",
        "postal_code": "13340",
        "phone": "29634957",
        "mobile_phone": "-",
        "extension_1": "021",
        "extension_2": "021",
        "fax": "29634957",
        "email": "-",
        "npwp": "-",
        "credit_limit": 0,
        "account_group_id": 1,
        "reconsiliation_account_id": 1,
        "planning_group_id": 1,
        "terms_of_payment_id": 1,
        "house_bank_id": 1,
        "currency_id": 1,
        "tax_classification_id": 1
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

API Financial Liquidity
-----------------------

Bagian ini menjelaskan bagaimana cara menjalankan API Financial Liquidity melalui Swagger. Endpoint yang tersedia di API Financial Liquidity ada empat, yakni **Get**, **Post**, **Get By Id**, dan **Put**.

Get
^^^

1. Buka endpoint Get di API Financial Liquidity, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

2. Isi parameter **display** dengan ``all`` untuk menampilkan semua data, atau ``page`` untuk menampilkan beberapa data.

3. Jika ingin menampilkan data dalam periode waktu tertentu, Anda juga dapat mengisi parameter **start-period** dan **finish-period** dengan format tanggal ``yyyy-mm-dd``.

4. Jika Anda memilih ``page``, Anda perlu mengisi parameter **limit** dengan angka sesuai dengan data yang ingin ditampilkan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-financials.png
  :width: 600
  :align: center

5. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial Liquidity. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": [
            {
                "id": 204,
                "company": {
                    "id": 2,
                    "name": "PP Induk"
                },
                "project_id": 1,
                "project_name": "421501 BENDUNGAN LOLAK - SULAWESI UTARA",
                "category_id": 2,
                "category_name": "PLAN",
                "flag_cf_id": 3,
                "flag_cf_name": "Overhead",
                "data_period_id": 3,
                "data_period_name": "YEAR",
                "bank_id": 1,
                "bank_name": "PT BANK RAKYAT INDONESIA (PERSERO) Tbk",
                "posting_date": "2021-11-23T00:00:00+07:00",
                "trading_partner": "",
                "plan_date": "Y1",
                "value": 311,
                "reference_id": "tw0kSiN0tkmNmqgWtu9S",
                "created_at": "2021-11-29T10:20:16+07:00",
                "updated_at": "2021-11-29T10:25:15+07:00"
            },
            {
                "id": 205,
                "company": {
                    "id": 2,
                    "name": "PP Induk"
                },
                "project_id": 1,
                "project_name": "421501 BENDUNGAN LOLAK - SULAWESI UTARA",
                "category_id": 2,
                "category_name": "PLAN",
                "flag_cf_id": 3,
                "flag_cf_name": "Overhead",
                "data_period_id": 3,
                "data_period_name": "YEAR",
                "bank_id": 1,
                "bank_name": "PT BANK RAKYAT INDONESIA (PERSERO) Tbk",
                "posting_date": "2021-11-23T00:00:00+07:00",
                "trading_partner": "",
                "plan_date": "Y2",
                "value": 302,
                "reference_id": "tw0kSiN0tkmNmqgWtu9S",
                "created_at": "2021-11-29T10:20:16+07:00",
                "updated_at": "2021-11-29T10:20:16+07:00"
            }
        ]
    }

Post
^^^^

1. Buka endpoint Post di API Financial Liquidity, lalu klik tombol **Try it out** yang berada di pojok kanan atas.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial Liquidity yang ingin disimpan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "company_id": 1,
        "project_id": 1,
        "category_id": 2,
        "flag_cf_id": 1,
        "data_period_id": 3,
        "bank_id": 1,
        "posting_date": "2021-12-08",
        "trading_partner_id": 1,
        "data_periods": [
            {
                "value": 100000
            },
            {
                "value": 100000
            },
            {
                "value": 100000
            },
            {
                "value": 100000
            },
            {
                "value": 100000
            }
        ]
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

Get By Id
^^^^^^^^^

1. Buka endpoint Get By Id di API Financial Liquidity, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial Liquidity yang ingin ditampilkan.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-by-id-liquidity.png
  :width: 600
  :align: center

3. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial Liquidity. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": {
            "id": 200,
            "company": {
            "id": 2,
            "name": "PP Induk"
            },
            "project_id": 1,
            "project_name": "421501 BENDUNGAN LOLAK - SULAWESI UTARA",
            "category_id": 2,
            "category_name": "PLAN",
            "flag_cf_id": 2,
            "flag_cf_name": "Vendor Payables",
            "data_period_id": 2,
            "data_period_name": "MONTH",
            "bank_id": 1,
            "bank_name": "PT BANK RAKYAT INDONESIA (PERSERO) Tbk",
            "posting_date": "2021-11-23T00:00:00+07:00",
            "trading_partner": "PT PP (Persero) Tbk",
            "plan_date": "M9",
            "value": 209,
            "reference_id": "N8DMEHWZTedL08ShGYNY",
            "created_at": "2021-11-29T10:16:04+07:00",
            "updated_at": "2021-11-29T10:16:04+07:00"
        }
    }

Put
^^^

1. Buka endpoint Put di API Financial Liquidity, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial Liquidity yang ingin diubah datanya.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial Liquidity yang ingin diubah. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "value": 20000000
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

API Financial Investment
------------------------

Bagian ini menjelaskan bagaimana cara menjalankan API Financial Investment melalui Swagger. Endpoint yang tersedia di API Financial Investment ada empat, yakni **Get**, **Post**, **Get By Id**, dan **Put**.

Get
^^^

1. Buka endpoint Get di API Financial Investment, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

2. Isi parameter **display** dengan ``all`` untuk menampilkan semua data, atau ``page`` untuk menampilkan beberapa data.

3. Jika ingin menampilkan data dalam periode waktu tertentu, Anda juga dapat mengisi parameter **start-period** dan **finish-period** dengan format tanggal ``yyyy-mm-dd``.

4. Jika Anda memilih ``page``, Anda perlu mengisi parameter **limit** dengan angka sesuai dengan data yang ingin ditampilkan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-financials.png
  :width: 600
  :align: center

5. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial Investment. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": [
            {
                "id": 81,
                "company": {
                    "id": 1,
                    "name": "PP Konsolidasi"
                },
                "project_id": 1,
                "project_name": "421501 BENDUNGAN LOLAK - SULAWESI UTARA",
                "category_id": 2,
                "category_name": "PLAN",
                "flag_investment_id": 1,
                "flag_investment_name": "CONTRACT VALUE",
                "bank_id": 1,
                "bank_name": "PT BANK RAKYAT INDONESIA (PERSERO) Tbk",
                "posting_date": "2021-11-29T00:00:00+07:00",
                "trading_partner": "PT PP (Persero) Tbk",
                "status": "IN PROGRESS",
                "value": 0,
                "reference_id": "hbs0mC9I7Y",
                "created_at": "2021-11-29T13:46:00+07:00",
                "updated_at": "2021-11-29T13:46:00+07:00"
            },
            {
                "id": 82,
                "company": {
                    "id": 1,
                    "name": "PP Konsolidasi"
                },
                "project_id": 1,
                "project_name": "421501 BENDUNGAN LOLAK - SULAWESI UTARA",
                "category_id": 2,
                "category_name": "PLAN",
                "flag_investment_id": 2,
                "flag_investment_name": "IRR",
                "bank_id": 1,
                "bank_name": "PT BANK RAKYAT INDONESIA (PERSERO) Tbk",
                "posting_date": "2021-11-29T00:00:00+07:00",
                "trading_partner": "PT PP (Persero) Tbk",
                "status": "IN PROGRESS",
                "value": 0,
                "reference_id": "hbs0mC9I7Y",
                "created_at": "2021-11-29T13:46:00+07:00",
                "updated_at": "2021-11-29T13:46:00+07:00"
            }
        ]
    }

Post
^^^^

1. Buka endpoint Post di API Financial Investment, lalu klik tombol **Try it out** yang berada di pojok kanan atas.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial Investment yang ingin disimpan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "company_id": 1,
        "project_id": 1,
        "bank_id": 1,
        "posting_date": "2021-12-09",
        "status": "IN PROGRESS",
        "trading_partner_id": 1,
        "data_investment_plan": [
            {
            "id": 1,
            "value": 3000000
            },
            {
            "id": 2,
            "value": 3000000
            },
            {
            "id": 3,
            "value": 3000000
            },
            {
            "id": 4,
            "value": 3000000
            },
            {
            "id": 5,
            "value": 3000000
            }
        ],
        "data_investment_actual": [
            {
            "id": 1,
            "value": 5000000
            },
            {
            "id": 2,
            "value": 5000000
            },
            {
            "id": 3,
            "value": 5000000
            },
            {
            "id": 4,
            "value": 5000000
            },
            {
            "id": 1,
            "value": 5000000
            }
        ]
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

Get By Id
^^^^^^^^^

1. Buka endpoint Get By Id di API Financial Investment, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial Investment yang ingin ditampilkan.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-by-id-investment.png
  :width: 600
  :align: center

3. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial Investment. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": {
            "id": 82,
            "company": {
                "id": 1,
                "name": "PP Konsolidasi"
            },
            "project_id": 1,
            "project_name": "421501 BENDUNGAN LOLAK - SULAWESI UTARA",
            "category_id": 2,
            "category_name": "PLAN",
            "flag_investment_id": 2,
            "flag_investment_name": "IRR",
            "bank_id": 1,
            "bank_name": "PT BANK RAKYAT INDONESIA (PERSERO) Tbk",
            "posting_date": "2021-11-29T00:00:00+07:00",
            "trading_partner": "PT PP (Persero) Tbk",
            "status": "IN PROGRESS",
            "value": 0,
            "reference_id": "hbs0mC9I7Y",
            "created_at": "2021-11-29T13:46:00+07:00",
            "updated_at": "2021-11-29T13:46:00+07:00"
        }
    }

Put
^^^

1. Buka endpoint Put di API Financial Investment, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial Investment yang ingin diubah datanya.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial Investment yang ingin diubah. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "value": 20000000
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

API Financial Days Sales Outstanding
------------------------------------

Bagian ini menjelaskan bagaimana cara menjalankan API Financial Days Sales Outstanding melalui Swagger. Endpoint yang tersedia di API Financial Days Sales Outstanding ada empat, yakni **Get**, **Post**, **Get By Id**, dan **Put**.

Get
^^^

1. Buka endpoint Get di API Financial Days Sales Outstanding, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

2. Isi parameter **display** dengan ``all`` untuk menampilkan semua data, atau ``page`` untuk menampilkan beberapa data.

3. Jika ingin menampilkan data dalam periode waktu tertentu, Anda juga dapat mengisi parameter **start-period** dan **finish-period** dengan format tanggal ``yyyy-mm-dd``.

4. Jika Anda memilih ``page``, Anda perlu mengisi parameter **limit** dengan angka sesuai dengan data yang ingin ditampilkan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-financials.png
  :width: 600
  :align: center

5. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial Days Sales Outstanding. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": [
            {
                "id": 31,
                "company": {
                    "id": 50,
                    "name": "PT PPRO Sampurna Jaya"
                },
                "project_id": 632,
                "project_name": "PT PPRO Sampurna Jaya",
                "category_id": 1,
                "category_name": "ACTUAL",
                "debt_type_id": 4,
                "debt_type_name": "ACCOUNT RECEIVABLES",
                "posting_date": "2021-11-26T00:00:00+07:00",
                "trading_partner": "PT PP (Persero) Tbk",
                "customer_id": 1,
                "customer_name": "KEMENTRIAN PEKERJAAN UMUM & PERUMAHAN RAKYAT RI",
                "aging_dso_id": 1,
                "aging_dso_name": "0-30 Days",
                "value": 0,
                "reference_id": "YYXnZaz4UF",
                "created_at": "2021-11-26T10:59:02+07:00",
                "updated_at": "2021-11-26T10:59:02+07:00"
            },
            {
                "id": 32,
                "company": {
                    "id": 50,
                    "name": "PT PPRO Sampurna Jaya"
                },
                "project_id": 632,
                "project_name": "PT PPRO Sampurna Jaya",
                "category_id": 1,
                "category_name": "ACTUAL",
                "debt_type_id": 4,
                "debt_type_name": "ACCOUNT RECEIVABLES",
                "posting_date": "2021-11-26T00:00:00+07:00",
                "trading_partner": "PT PP (Persero) Tbk",
                "customer_id": 1,
                "customer_name": "KEMENTRIAN PEKERJAAN UMUM & PERUMAHAN RAKYAT RI",
                "aging_dso_id": 2,
                "aging_dso_name": "31-60 Days",
                "value": 0,
                "reference_id": "YYXnZaz4UF",
                "created_at": "2021-11-26T10:59:02+07:00",
                "updated_at": "2021-11-26T10:59:02+07:00"
            }
        ]
    }

Post
^^^^

1. Buka endpoint Post di API Financial Days Sales Outstanding, lalu klik tombol **Try it out** yang berada di pojok kanan atas.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial Days Sales Outstanding yang ingin disimpan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "company_id": 1,
        "project_id": 1,
        "posting_date": "2021-12-09",
        "trading_partner_id": 1,
        "customer_id": 1,
        "data_aging_dso": [
            {
            "id": 1,
            "value": 100000
            },
            {
            "id": 2,
            "value": 100000
            },
            {
            "id": 3,
            "value": 100000
            },
            {
            "id": 4,
            "value": 100000
            },
            {
            "id": 5,
            "value": 100000
            },
            {
            "id": 6,
            "value": 100000
            }
        ]
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

Get By Id
^^^^^^^^^

1. Buka endpoint Get By Id di API Financial Days Sales Outstanding, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial Days Sales Outstanding yang ingin ditampilkan.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-by-id-days-sales-outstanding.png
  :width: 600
  :align: center

3. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial Days Sales Outstanding. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": {
            "id": 31,
            "company": {
            "id": 50,
            "name": "PT PPRO Sampurna Jaya"
            },
            "project_id": 632,
            "project_name": "PT PPRO Sampurna Jaya",
            "category_id": 1,
            "category_name": "ACTUAL",
            "debt_type_id": 4,
            "debt_type_name": "ACCOUNT RECEIVABLES",
            "posting_date": "2021-11-26T00:00:00+07:00",
            "trading_partner": "PT PP (Persero) Tbk",
            "customer_id": 1,
            "customer_name": "KEMENTRIAN PEKERJAAN UMUM & PERUMAHAN RAKYAT RI",
            "aging_dso_id": 1,
            "aging_dso_name": "0-30 Days",
            "value": 0,
            "reference_id": "YYXnZaz4UF",
            "created_at": "2021-11-26T10:59:02+07:00",
            "updated_at": "2021-11-26T10:59:02+07:00"
        }
    }

Put
^^^

1. Buka endpoint Put di API Financial Days Sales Outstanding, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial Days Sales Outstanding yang ingin diubah datanya.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial Days Sales Outstanding yang ingin diubah. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "value": 20000000
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

API Financial Term Loan
-----------------------

Bagian ini menjelaskan bagaimana cara menjalankan API Financial Term Loan melalui Swagger. Endpoint yang tersedia di API Financial Term Loan ada empat, yakni **Get**, **Post**, **Get By Id**, dan **Put**.

Get
^^^

1. Buka endpoint Get di API Financial Term Loan, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

2. Isi parameter **display** dengan ``all`` untuk menampilkan semua data, atau ``page`` untuk menampilkan beberapa data.

3. Jika ingin menampilkan data dalam periode waktu tertentu, Anda juga dapat mengisi parameter **start-period** dan **finish-period** dengan format tanggal ``yyyy-mm-dd``.

4. Jika Anda memilih ``page``, Anda perlu mengisi parameter **limit** dengan angka sesuai dengan data yang ingin ditampilkan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-financials.png
  :width: 600
  :align: center

5. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial Term Loan. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": [
            {
                "id": 35,
                "company": {
                    "id": 1,
                    "name": "PP Konsolidasi"
                },
                "project_id": 1,
                "project_name": "421501 BENDUNGAN LOLAK - SULAWESI UTARA",
                "category_id": 1,
                "category_name": "ACTUAL",
                "debt_type_id": 1,
                "debt_type_name": "TERM LOAN",
                "bank_id": 1,
                "bank_name": "PT BANK RAKYAT INDONESIA (PERSERO) Tbk",
                "source_group_id": 1,
                "source_group_name": "LOCAL BANK",
                "debt_payment_id": 2,
                "debt_payment_name": "NOT LATE",
                "posting_date": "2021-12-01",
                "trading_partner": "PT PP (Persero) Tbk",
                "debt_name": "TEST TERM LOAN 1",
                "start_duration_date": "2021-12-01",
                "maturity_date": "2021-12-01T00:00:00+07:00",
                "interest_rate": 1,
                "value": 3000000,
                "type": "MATURITY",
                "total": 0,
                "reference_id": "9t2v0c79JW",
                "created_at": "2021-12-01T13:17:57+07:00",
                "updated_at": "2021-12-01T13:17:57+07:00"
            }
        ]
    }

Post
^^^^

1. Buka endpoint Post di API Financial Term Loan, lalu klik tombol **Try it out** yang berada di pojok kanan atas.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial Term Loan yang ingin disimpan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "company_id": 1,
        "project_id": 1,
        "posting_date": "2021-12-09",
        "trading_partner_id": 1,
        "bank_id": 1,
        "source_group_id": 1,
        "debt_payment_id": 1,
        "type": "MATURITY",
        "total": 0,
        "debt_data": [
            {
                "debt_name": "TEST TERM LOAN 001",
                "start_duration": "2021-12-01",
                "maturity_date": "2021-12-31",
                "interest_rate": 1,
                "value": 35000000
            },
            {
                "debt_name": "TEST TERM LOAN 002",
                "start_duration": "2021-12-01",
                "maturity_date": "2021-12-31",
                "interest_rate": 2,
                "value": 35000000
            }
        ]
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

Get By Id
^^^^^^^^^

1. Buka endpoint Get By Id di API Financial Term Loan, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial Term Loan yang ingin ditampilkan.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-by-id-term-loan.png
  :width: 600
  :align: center

3. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial Term Loan. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": {
            "id": 35,
            "company": {
                "id": 1,
                "name": "PP Konsolidasi"
            },
            "project_id": 1,
            "project_name": "421501 BENDUNGAN LOLAK - SULAWESI UTARA",
            "category_id": 1,
            "category_name": "ACTUAL",
            "debt_type_id": 1,
            "debt_type_name": "TERM LOAN",
            "bank_id": 1,
            "bank_name": "PT BANK RAKYAT INDONESIA (PERSERO) Tbk",
            "source_group_id": 1,
            "source_group_name": "LOCAL BANK",
            "debt_payment_id": 2,
            "debt_payment_name": "NOT LATE",
            "posting_date": "2021-12-01T00:00:00+07:00",
            "trading_partner": "PT PP (Persero) Tbk",
            "debt_name": "TEST TERM LOAN 1",
            "start_duration_date": "2021-12-01T00:00:00+07:00",
            "maturity_date": "2021-12-01T00:00:00+07:00",
            "interest_rate": 1,
            "value": 3000000,
            "type": "MATURITY",
            "total": 0,
            "reference_id": "9t2v0c79JW",
            "created_at": "2021-12-01T13:17:57+07:00",
            "updated_at": "2021-12-01T13:17:57+07:00"
        }
    }

Put
^^^

1. Buka endpoint Put di API Financial Term Loan, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial Term Loan yang ingin diubah datanya.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial Term Loan yang ingin diubah. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "value": 20000000
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

API Financial Term Loan Allocation
----------------------------------

Bagian ini menjelaskan bagaimana cara menjalankan API Financial Term Loan Allocation melalui Swagger. Endpoint yang tersedia di API Financial Term Loan Allocation ada empat, yakni **Get**, **Post**, **Get By Id**, dan **Put**.

Get
^^^

1. Buka endpoint Get di API Financial Term Loan Allocation, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

2. Isi parameter **display** dengan ``all`` untuk menampilkan semua data, atau ``page`` untuk menampilkan beberapa data.

3. Jika ingin menampilkan data dalam periode waktu tertentu, Anda juga dapat mengisi parameter **start-period** dan **finish-period** dengan format tanggal ``yyyy-mm-dd``.

4. Jika Anda memilih ``page``, Anda perlu mengisi parameter **limit** dengan angka sesuai dengan data yang ingin ditampilkan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-financials.png
  :width: 600
  :align: center

5. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial Term Loan Allocation. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": [
            {
                "id": 27,
                "company": {
                    "id": 1,
                    "name": "PP Konsolidasi"
                },
                "project_id": 1,
                "project_name": "421501 BENDUNGAN LOLAK - SULAWESI UTARA",
                "category_id": 1,
                "category_name": "ACTUAL",
                "trading_partner_id": 1,
                "trading_partner_name": "PT PP (Persero) Tbk",
                "posting_date": "2022-01-01T00:00:00+07:00",
                "financial_term_loan_id": 35,
                "financial_term_loan_name": "TEST TERM LOAN 1",
                "bank_name": "PT BANK RAKYAT INDONESIA (PERSERO) Tbk",
                "source_group_name": "LOCAL BANK",
                "total": 0,
                "debt_payment_name": "NOT LATE",
                "debt_type_name": "TERM LOAN",
                "debt_allocation_group_id": 1,
                "debt_allocation_group_name": "CONSTRUCTION PROJECT",
                "result_allocation_name": "318002 - APRON BANDARA I GUSTI NGURAH RAI BALI",
                "value": -3000000,
                "is_reverse": 1,
                "reference_id": "jQS5Xszm02",
                "created_at": "2021-12-02T13:13:22+07:00",
                "updated_at": "2021-12-02T13:13:22+07:00"
            },
            {
                "id": 28,
                "company": {
                    "id": 1,
                    "name": "PP Konsolidasi"
                },
                "project_id": 1,
                "project_name": "421501 BENDUNGAN LOLAK - SULAWESI UTARA",
                "category_id": 1,
                "category_name": "ACTUAL",
                "trading_partner_id": 1,
                "trading_partner_name": "PT PP (Persero) Tbk",
                "posting_date": "2022-01-01T00:00:00+07:00",
                "financial_term_loan_id": 35,
                "financial_term_loan_name": "TEST TERM LOAN 1",
                "bank_name": "PT BANK RAKYAT INDONESIA (PERSERO) Tbk",
                "source_group_name": "LOCAL BANK",
                "total": 0,
                "debt_payment_name": "NOT LATE",
                "debt_type_name": "TERM LOAN",
                "debt_allocation_group_id": 1,
                "debt_allocation_group_name": "CONSTRUCTION PROJECT",
                "result_allocation_name": "318002 - APRON BANDARA I GUSTI NGURAH RAI BALI",
                "value": 3000000,
                "is_reverse": 1,
                "reference_id": "jQS5Xszm02",
                "created_at": "2021-12-02T13:13:22+07:00",
                "updated_at": "2021-12-02T13:13:22+07:00"
            }
        ]
    }

Post
^^^^

1. Buka endpoint Post di API Financial Term Loan Allocation, lalu klik tombol **Try it out** yang berada di pojok kanan atas.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial Term Loan Allocation yang ingin disimpan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "company_id": 1,
        "project_id": 1,
        "posting_date": "2021-12-10",
        "trading_partner_id": 1,
        "debt_id": 1,
        "debt_allocation_group": [
            {
                "debt_allocation_group_id": 1,
                "result_allocation_group_id": 100,
                "value": 70000000
            }
        ]
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

Get By Id
^^^^^^^^^

1. Buka endpoint Get By Id di API Financial Term Loan Allocation, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial Term Loan Allocation yang ingin ditampilkan.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-by-id-term-loan-allocation.png
  :width: 600
  :align: center

3. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial Term Loan Allocation. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": {
            "id": 27,
            "company": {
                "id": 1,
                "name": "PP Konsolidasi"
            },
            "project_id": 1,
            "project_name": "421501 BENDUNGAN LOLAK - SULAWESI UTARA",
            "category_id": 1,
            "category_name": "ACTUAL",
            "trading_partner_id": 1,
            "trading_partner_name": "PT PP (Persero) Tbk",
            "posting_date": "2022-01-01T00:00:00+07:00",
            "financial_term_loan_id": 35,
            "financial_term_loan_name": "TEST TERM LOAN 1",
            "bank_name": "PT BANK RAKYAT INDONESIA (PERSERO) Tbk",
            "source_group_name": "LOCAL BANK",
            "total": 0,
            "debt_payment_name": "NOT LATE",
            "debt_type_name": "TERM LOAN",
            "debt_allocation_group_id": 1,
            "debt_allocation_group_name": "CONSTRUCTION PROJECT",
            "result_allocation_name": "318002 - APRON BANDARA I GUSTI NGURAH RAI BALI",
            "value": -3000000,
            "is_reverse": 1,
            "reference_id": "jQS5Xszm02",
            "created_at": "2021-12-02T13:13:22+07:00",
            "updated_at": "2021-12-02T13:13:22+07:00"
        }
    }

Put
^^^

1. Buka endpoint Put di API Financial Term Loan Allocation, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial Term Loan Allocation yang ingin diubah datanya.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial Term Loan Allocation yang ingin diubah. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "value": 20000000
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

API Financial Reverse Term Loan Allocation
------------------------------------------

Bagian ini menjelaskan bagaimana cara menjalankan API Financial Reverse Term Loan Allocation melalui Swagger. Endpoint yang tersedia di API Financial Reverse Term Loan Allocation hanya ada satu, yakni **Postt**.

Post
^^^^

1. Buka endpoint Post di API Financial Reverse Term Loan Allocation, lalu klik tombol **Try it out** yang berada di pojok kanan atas.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial Term Loan Allocation yang ingin disimpan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "company_id": 1,
        "financial_term_loan_id": 20
    }

API Financial Revolving Loan
----------------------------

Bagian ini menjelaskan bagaimana cara menjalankan API Financial Revolving Loan melalui Swagger. Endpoint yang tersedia di API Financial Revolving Loan ada empat, yakni **Get**, **Post**, **Get By Id**, dan **Put**.

Get
^^^

1. Buka endpoint Get di API Financial Revolving Loan, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

2. Isi parameter **display** dengan ``all`` untuk menampilkan semua data, atau ``page`` untuk menampilkan beberapa data.

3. Jika ingin menampilkan data dalam periode waktu tertentu, Anda juga dapat mengisi parameter **start-period** dan **finish-period** dengan format tanggal ``yyyy-mm-dd``.

4. Jika Anda memilih ``page``, Anda perlu mengisi parameter **limit** dengan angka sesuai dengan data yang ingin ditampilkan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-financials.png
  :width: 600
  :align: center

5. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial Revolving Loan. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": [
            {
            "id": 8,
            "company": {
                "id": 1,
                "name": "PP Konsolidasi"
            },
            "project_id": 1,
            "project_name": "421501 BENDUNGAN LOLAK - SULAWESI UTARA",
            "category_id": 1,
            "category_name": "ACTUAL",
            "posting_date": "2021-12-01T00:00:00+07:00",
            "trading_partner_id": 1,
            "trading_partner_name": "PT PP (Persero) Tbk",
            "bank_id": 1,
            "bank_name": "PT BANK RAKYAT INDONESIA (PERSERO) Tbk",
            "source_group_id": 1,
            "source_group_name": "LOCAL BANK",
            "debt_type_id": 2,
            "debt_type_name": "REVOLVING LOAN",
            "debt_name": "TEST EDIT REVOLVING LOAN",
            "credit_loan_opening_date": "2021-12-02T00:00:00+07:00",
            "maturity_date": "2021-12-02T00:00:00+07:00",
            "interest_rate": 2,
            "plafon_value": 50000000,
            "debt_equity": 2,
            "debt_ebitda": 2,
            "iscr": 2,
            "debt_payment_id": 2,
            "debt_payment_name": "NOT LATE",
            "reference_id": "TSpgePXyJrEPnr6M5bCY",
            "created_at": "2021-12-01T13:52:10+07:00",
            "updated_at": "2021-12-01T13:54:23+07:00"
            }
        ]
    }

Post
^^^^

1. Buka endpoint Post di API Financial Revolving Loan, lalu klik tombol **Try it out** yang berada di pojok kanan atas.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial Revolving Loan yang ingin disimpan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "company_id": 1,
        "project_id": 1,
        "posting_date": "2021-12-10",
        "trading_partner_id": 1,
        "bank_id": 1,
        "source_group_id": 1,
        "revolving_loan_data": [
            {
                "loan_name": "TEST REVOLVING LOAN 001",
                "credit_loan_opening_date": "2021-12-10",
                "maturity_date": "2021-12-10",
                "interest_rate": 1,
                "plafon_value": 900000000,
                "debt_quity": 1,
                "debt_ebita": 1,
                "iscr": 1,
                "debt_payment_id": 1
            }
        ]
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

Get By Id
^^^^^^^^^

1. Buka endpoint Get By Id di API Financial Revolving Loan, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial Revolving Loan yang ingin ditampilkan.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-by-id-revolving-loan.png
  :width: 600
  :align: center

3. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial Revolving Loan. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": {
            "id": 8,
            "company": {
                "id": 1,
                "name": "PP Konsolidasi"
            },
            "project_id": 1,
            "project_name": "421501 BENDUNGAN LOLAK - SULAWESI UTARA",
            "category_id": 1,
            "category_name": "ACTUAL",
            "posting_date": "2021-12-01T00:00:00+07:00",
            "trading_partner_id": 1,
            "trading_partner_name": "PT PP (Persero) Tbk",
            "bank_id": 1,
            "bank_name": "PT BANK RAKYAT INDONESIA (PERSERO) Tbk",
            "source_group_id": 1,
            "source_group_name": "LOCAL BANK",
            "debt_type_id": 2,
            "debt_type_name": "REVOLVING LOAN",
            "debt_name": "TEST EDIT REVOLVING LOAN",
            "credit_loan_opening_date": "2021-12-02T00:00:00+07:00",
            "maturity_date": "2021-12-02T00:00:00+07:00",
            "interest_rate": 2,
            "plafon_value": 50000000,
            "debt_equity": 2,
            "debt_ebitda": 2,
            "iscr": 2,
            "debt_payment_id": 2,
            "debt_payment_name": "NOT LATE",
            "reference_id": "TSpgePXyJrEPnr6M5bCY",
            "created_at": "2021-12-01T13:52:10+07:00",
            "updated_at": "2021-12-01T13:54:23+07:00"
        }
    }

Put
^^^

1. Buka endpoint Put di API Financial Revolving Loan, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial Revolving Loan yang ingin diubah datanya.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial Revolving Loan yang ingin diubah. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "value": 20000000
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

API Financial Revolving Drawdown
--------------------------------

Bagian ini menjelaskan bagaimana cara menjalankan API Financial Revolving Drawdown melalui Swagger. Endpoint yang tersedia di API Financial Revolving Drawdown ada empat, yakni **Get**, **Post**, **Get By Id**, dan **Put**.

Get
^^^

1. Buka endpoint Get di API Financial Revolving Drawdown, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

2. Isi parameter **display** dengan ``all`` untuk menampilkan semua data, atau ``page`` untuk menampilkan beberapa data.

3. Jika ingin menampilkan data dalam periode waktu tertentu, Anda juga dapat mengisi parameter **start-period** dan **finish-period** dengan format tanggal ``yyyy-mm-dd``.

4. Jika Anda memilih ``page``, Anda perlu mengisi parameter **limit** dengan angka sesuai dengan data yang ingin ditampilkan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-financials.png
  :width: 600
  :align: center

5. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial Revolving Drawdown. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": [
            {
                "id": 10,
                "company": {
                    "id": 1,
                    "name": "PP Konsolidasi"
                },
                "project_id": 1,
                "project_name": "421501 BENDUNGAN LOLAK - SULAWESI UTARA",
                "category_id": 1,
                "category_name": "ACTUAL",
                "posting_date": "2021-12-01T00:00:00+07:00",
                "trading_partner_id": 1,
                "trading_partner_name": "PT PP (Persero) Tbk",
                "revolving_loan_id": 8,
                "revolving_loan_name": "TEST EDIT REVOLVING LOAN",
                "debt_type_id": 2,
                "debt_type_name": "REVOLVING LOAN",
                "bank_id": 1,
                "bank_name": "PT BANK RAKYAT INDONESIA (PERSERO) Tbk",
                "source_group_id": 1,
                "source_group_name": "LOCAL BANK",
                "plafon_value": 50000000,
                "credit_line_detail": "TEST EDIT DRAWDOWN 123",
                "credit_line_opening_date": "2021-12-31T00:00:00+07:00",
                "maturity_date": "2021-12-31T00:00:00+07:00",
                "transaction_type": "PEMBAYARAN",
                "value": -25000000,
                "is_reverse": 1,
                "reference_id": "ZjLDdNxpedvEJy34d1mJ",
                "created_at": "2021-12-08T13:42:13+07:00",
                "updated_at": "2021-12-08T13:42:13+07:00"
            },
            {
                "id": 9,
                "company": {
                    "id": 1,
                    "name": "PP Konsolidasi"
                },
                "project_id": 1,
                "project_name": "421501 BENDUNGAN LOLAK - SULAWESI UTARA",
                "category_id": 1,
                "category_name": "ACTUAL",
                "posting_date": "2021-12-01T00:00:00+07:00",
                "trading_partner_id": 1,
                "trading_partner_name": "PT PP (Persero) Tbk",
                "revolving_loan_id": 8,
                "revolving_loan_name": "TEST EDIT REVOLVING LOAN",
                "debt_type_id": 2,
                "debt_type_name": "REVOLVING LOAN",
                "bank_id": 1,
                "bank_name": "PT BANK RAKYAT INDONESIA (PERSERO) Tbk",
                "source_group_id": 1,
                "source_group_name": "LOCAL BANK",
                "plafon_value": 50000000,
                "credit_line_detail": "TEST EDIT DRAWDOWN 123",
                "credit_line_opening_date": "2021-12-31T00:00:00+07:00",
                "maturity_date": "2021-12-31T00:00:00+07:00",
                "transaction_type": "PEMBAYARAN",
                "value": -25000000,
                "is_reverse": 1,
                "reference_id": "ZjLDdNxpedvEJy34d1mJ",
                "created_at": "2021-12-07T10:14:47+07:00",
                "updated_at": "2021-12-07T10:14:47+07:00"
            }
        ]
    }

Post
^^^^

1. Buka endpoint Post di API Financial Revolving Drawdown, lalu klik tombol **Try it out** yang berada di pojok kanan atas.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial Revolving Drawdown yang ingin disimpan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "company_id": 1,
        "revolving_loan_id": 1,
        "posting_date": "2021-12-10",
        "trading_partner_id": 1,
        "bank_id": 1,
        "source_group_id": 1,
        "revolving_drawdown_data": [
            {
                "credit_line_detail": "TEST REVOLVING DRAWDOWN",
                "credit_loan_opening_date": "2021-12-10",
                "maturity_date": "2021-12-10",
                "transaction_type": "PENCAIRAN",
                "value": 100000000
            }
        ]
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

Get By Id
^^^^^^^^^

1. Buka endpoint Get By Id di API Financial Revolving Drawdown, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial Revolving Drawdown yang ingin ditampilkan.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-by-id-revolving-drawdown.png
  :width: 600
  :align: center

3. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial Revolving Drawdown. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": {
            "id": 10,
            "company": {
                "id": 1,
                "name": "PP Konsolidasi"
            },
            "project_id": 1,
            "project_name": "421501 BENDUNGAN LOLAK - SULAWESI UTARA",
            "category_id": 1,
            "category_name": "ACTUAL",
            "posting_date": "2021-12-01T00:00:00+07:00",
            "trading_partner_id": 1,
            "trading_partner_name": "PT PP (Persero) Tbk",
            "revolving_loan_id": 8,
            "revolving_loan_name": "TEST EDIT REVOLVING LOAN",
            "debt_type_id": 2,
            "debt_type_name": "REVOLVING LOAN",
            "bank_id": 1,
            "bank_name": "PT BANK RAKYAT INDONESIA (PERSERO) Tbk",
            "source_group_id": 1,
            "source_group_name": "LOCAL BANK",
            "plafon_value": 50000000,
            "credit_line_detail": "TEST EDIT DRAWDOWN 123",
            "credit_line_opening_date": "2021-12-31T00:00:00+07:00",
            "maturity_date": "2021-12-31T00:00:00+07:00",
            "transaction_type": "PEMBAYARAN",
            "value": -25000000,
            "is_reverse": 1,
            "reference_id": "ZjLDdNxpedvEJy34d1mJ",
            "created_at": "2021-12-08T13:42:13+07:00",
            "updated_at": "2021-12-08T13:42:13+07:00"
        }
    }

Put
^^^

1. Buka endpoint Put di API Financial Revolving Drawdown, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial Revolving Drawdown yang ingin diubah datanya.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial Revolving Drawdown yang ingin diubah. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "value": 20000000
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

API Financial Reverse Revolving Drawdown
----------------------------------------

Bagian ini menjelaskan bagaimana cara menjalankan API Financial Reverse Revolving Drawdown melalui Swagger. Endpoint yang tersedia di API Financial Reverse Revolving Drawdown hanya ada satu, yakni **Postt**.

Post
^^^^

1. Buka endpoint Post di API Financial Reverse Revolving Drawdown, lalu klik tombol **Try it out** yang berada di pojok kanan atas.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial Revolving Drawdown yang ingin disimpan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "company_id": 1,
        "financial_revolving_drawdown_id": 10
    }

API Financial Revolving Allocation
----------------------------------

Bagian ini menjelaskan bagaimana cara menjalankan API Financial Revolving Allocation melalui Swagger. Endpoint yang tersedia di API Financial Revolving Allocation ada empat, yakni **Get**, **Post**, **Get By Id**, dan **Put**.

Get
^^^

1. Buka endpoint Get di API Financial Revolving Allocation, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

2. Isi parameter **display** dengan ``all`` untuk menampilkan semua data, atau ``page`` untuk menampilkan beberapa data.

3. Jika ingin menampilkan data dalam periode waktu tertentu, Anda juga dapat mengisi parameter **start-period** dan **finish-period** dengan format tanggal ``yyyy-mm-dd``.

4. Jika Anda memilih ``page``, Anda perlu mengisi parameter **limit** dengan angka sesuai dengan data yang ingin ditampilkan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-financials.png
  :width: 600
  :align: center

5. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial Revolving Allocation. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": [
            {
                "id": 6,
                "company": {
                    "id": 1,
                    "name": "PP Konsolidasi"
                },
                "project_id": 1,
                "project_name": "421501 BENDUNGAN LOLAK - SULAWESI UTARA",
                "category_id": 1,
                "category_name": "ACTUAL",
                "trading_partner_id": 1,
                "trading_partner_name": "PT PP (Persero) Tbk",
                "revolving_drawdown_id": 8,
                "revolving_drawdown_name": "TEST EDIT DRAWDOWN 123",
                "posting_date": "2021-12-08T00:00:00+07:00",
                "debt_allocation_group_id": 2,
                "debt_allocation_group_name": "SHAREHOLDER LOANS",
                "result_allocation_name": "PP03 - PP Properti Tbk",
                "value": 20000000,
                "is_reverse": 0,
                "reference_id": "Xz0gkZZdq9mKXkH0kN1C",
                "created_at": "2021-12-08T13:44:12+07:00",
                "updated_at": "2021-12-08T13:46:12+07:00"
            },
            {
                "id": 5,
                "company": {
                    "id": 1,
                    "name": "PP Konsolidasi"
                },
                "project_id": 1,
                "project_name": "421501 BENDUNGAN LOLAK - SULAWESI UTARA",
                "category_id": 1,
                "category_name": "ACTUAL",
                "trading_partner_id": 1,
                "trading_partner_name": "PT PP (Persero) Tbk",
                "revolving_drawdown_id": 8,
                "revolving_drawdown_name": "TEST EDIT DRAWDOWN 123",
                "posting_date": "2021-12-07T00:00:00+07:00",
                "debt_allocation_group_id": 1,
                "debt_allocation_group_name": "CONSTRUCTION PROJECT",
                "result_allocation_name": "381501 - 421501 BENDUNGAN LOLAK - SULAWESI UTARA",
                "value": 15000000,
                "is_reverse": 0,
                "reference_id": "OUJtzie9ufaoW4PQ2KiE",
                "created_at": "2021-12-07T10:14:19+07:00",
                "updated_at": "2021-12-07T10:14:19+07:00"
            }
        ]
    }

Post
^^^^

1. Buka endpoint Post di API Financial Revolving Allocation, lalu klik tombol **Try it out** yang berada di pojok kanan atas.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial Revolving Allocation yang ingin disimpan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "company_id": 1,
        "project_id": 1,
        "posting_date": "2021-12-10",
        "trading_partner_id": 1,
        "revolving_drawdown_id": 10,
        "debt_allocation_group": [
            {
                "debt_allocation_group_id": 1,
                "result_allocation_group_id": 100,
                "value": 500000000
            }
        ]
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

Get By Id
^^^^^^^^^

1. Buka endpoint Get By Id di API Financial Revolving Allocation, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial Revolving Allocation yang ingin ditampilkan.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-by-id-revolving-allocation.png
  :width: 600
  :align: center

3. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial Revolving Allocation. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": {
            "id": 6,
            "company": {
                "id": 1,
                "name": "PP Konsolidasi"
            },
            "project_id": 1,
            "project_name": "421501 BENDUNGAN LOLAK - SULAWESI UTARA",
            "category_id": 1,
            "category_name": "ACTUAL",
            "trading_partner_id": 1,
            "trading_partner_name": "PT PP (Persero) Tbk",
            "revolving_drawdown_id": 8,
            "revolving_drawdown_name": "TEST EDIT DRAWDOWN 123",
            "posting_date": "2021-12-08T00:00:00+07:00",
            "debt_allocation_group_id": 2,
            "debt_allocation_group_name": "SHAREHOLDER LOANS",
            "result_allocation_name": "PP03 - PP Properti Tbk",
            "value": 20000000,
            "is_reverse": 0,
            "reference_id": "Xz0gkZZdq9mKXkH0kN1C",
            "created_at": "2021-12-08T13:44:12+07:00",
            "updated_at": "2021-12-08T13:46:12+07:00"
        }
    }

Put
^^^

1. Buka endpoint Put di API Financial Revolving Allocation, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial Revolving Allocation yang ingin diubah datanya.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial Revolving Allocation yang ingin diubah. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "value": 20000000
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

API Financial NCL
-----------------

Bagian ini menjelaskan bagaimana cara menjalankan API Financial NCL melalui Swagger. Endpoint yang tersedia di API Financial NCL ada empat, yakni **Get**, **Post**, **Get By Id**, dan **Put**.

Get
^^^

1. Buka endpoint Get di API Financial NCL, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

2. Isi parameter **display** dengan ``all`` untuk menampilkan semua data, atau ``page`` untuk menampilkan beberapa data.

3. Jika ingin menampilkan data dalam periode waktu tertentu, Anda juga dapat mengisi parameter **start-period** dan **finish-period** dengan format tanggal ``yyyy-mm-dd``.

4. Jika Anda memilih ``page``, Anda perlu mengisi parameter **limit** dengan angka sesuai dengan data yang ingin ditampilkan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-financials.png
  :width: 600
  :align: center

5. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial NCL. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": [
            {
                "id": 3,
                "company": {
                    "id": 1,
                    "name": "PP Konsolidasi"
                },
                "project_id": 1,
                "project_name": "421501 BENDUNGAN LOLAK - SULAWESI UTARA",
                "category_id": 1,
                "category_name": "ACTUAL",
                "trading_partner_id": 1,
                "trading_partner_name": "PT PP (Persero) Tbk",
                "bank_id": 1,
                "bank_name": "PT BANK RAKYAT INDONESIA (PERSERO) Tbk",
                "source_group_id": 1,
                "source_group_name": "LOCAL BANK",
                "debt_type_id": 3,
                "debt_type_name": "NCL",
                "posting_date": "2021-12-08T00:00:00+07:00",
                "debt_name": "TEST EDIT NCL 003",
                "credit_loan_opening_date": "2022-01-01T00:00:00+07:00",
                "maturity_date": "2022-01-01T00:00:00+07:00",
                "interest_rate": 2,
                "value": 800000000,
                "debt_equity": 2,
                "debt_ebitda": 2,
                "iscr": 2,
                "debt_payment_id": 2,
                "debt_payment_name": "NOT LATE",
                "reference_id": "GW0fqsZhuMWCMYhwUx9p",
                "created_at": "2021-12-08T13:48:39+07:00",
                "updated_at": "2021-12-08T13:50:44+07:00"
            },
            {
                "id": 2,
                "company": {
                    "id": 53,
                    "name": "PT Limasland Realty Cilegon"
                },
                "project_id": 635,
                "project_name": "PT Limasland Realty Cilegon",
                "category_id": 1,
                "category_name": "ACTUAL",
                "trading_partner_id": 2,
                "trading_partner_name": "PP Presisi",
                "bank_id": 1,
                "bank_name": "PT BANK RAKYAT INDONESIA (PERSERO) Tbk",
                "source_group_id": 1,
                "source_group_name": "LOCAL BANK",
                "debt_type_id": 3,
                "debt_type_name": "NCL",
                "posting_date": "2021-11-26T00:00:00+07:00",
                "debt_name": "sdfhjh",
                "credit_loan_opening_date": "2021-11-26T00:00:00+07:00",
                "maturity_date": "2021-11-26T00:00:00+07:00",
                "interest_rate": 1,
                "value": 1000000,
                "debt_equity": 1,
                "debt_ebitda": 1,
                "iscr": 1,
                "debt_payment_id": 2,
                "debt_payment_name": "NOT LATE",
                "reference_id": "l9xim1JNLG",
                "created_at": "2021-11-26T08:48:40+07:00",
                "updated_at": "2021-11-26T08:48:40+07:00"
            }
        ]
    }

Post
^^^^

1. Buka endpoint Post di API Financial NCL, lalu klik tombol **Try it out** yang berada di pojok kanan atas.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial NCL yang ingin disimpan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "company_id": 1,
        "project_id": 1,
        "posting_date": "2021-12-10",
        "trading_partner_id": 1,
        "bank_id": 1,
        "source_group_id": 1,
        "ncl_data": [
            {
                "loan_name": "TEST NCL 001",
                "credit_loan_opening_date": "2021-12-10",
                "maturity_date": "2021-12-10",
                "interest_rate": 1,
                "plafon_value": 355000000,
                "debt_quity": 1,
                "debt_ebita": 1,
                "iscr": 1,
                "debt_payment_id": 1
            }
        ]
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

Get By Id
^^^^^^^^^

1. Buka endpoint Get By Id di API Financial NCL, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial NCL yang ingin ditampilkan.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-by-id-ncl.png
  :width: 600
  :align: center

3. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial NCL. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": {
            "id": 3,
            "company": {
                "id": 1,
                "name": "PP Konsolidasi"
            },
            "project_id": 1,
            "project_name": "421501 BENDUNGAN LOLAK - SULAWESI UTARA",
            "category_id": 1,
            "category_name": "ACTUAL",
            "trading_partner_id": 1,
            "trading_partner_name": "PT PP (Persero) Tbk",
            "bank_id": 1,
            "bank_name": "PT BANK RAKYAT INDONESIA (PERSERO) Tbk",
            "source_group_id": 1,
            "source_group_name": "LOCAL BANK",
            "debt_type_id": 3,
            "debt_type_name": "NCL",
            "posting_date": "2021-12-08T00:00:00+07:00",
            "debt_name": "TEST EDIT NCL 003",
            "credit_loan_opening_date": "2022-01-01T00:00:00+07:00",
            "maturity_date": "2022-01-01T00:00:00+07:00",
            "interest_rate": 2,
            "value": 800000000,
            "debt_equity": 2,
            "debt_ebitda": 2,
            "iscr": 2,
            "debt_payment_id": 2,
            "debt_payment_name": "NOT LATE",
            "reference_id": "GW0fqsZhuMWCMYhwUx9p",
            "created_at": "2021-12-08T13:48:39+07:00",
            "updated_at": "2021-12-08T13:50:44+07:00"
        }
    }

Put
^^^

1. Buka endpoint Put di API Financial NCL, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial NCL yang ingin diubah datanya.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial NCL yang ingin diubah. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "value": 20000000
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

API Financial NCL Drawdown
--------------------------

Bagian ini menjelaskan bagaimana cara menjalankan API Financial NCL Drawdown melalui Swagger. Endpoint yang tersedia di API Financial NCL Drawdown ada empat, yakni **Get**, **Post**, **Get By Id**, dan **Put**.

Get
^^^

1. Buka endpoint Get di API Financial NCL Drawdown, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

2. Isi parameter **display** dengan ``all`` untuk menampilkan semua data, atau ``page`` untuk menampilkan beberapa data.

3. Jika ingin menampilkan data dalam periode waktu tertentu, Anda juga dapat mengisi parameter **start-period** dan **finish-period** dengan format tanggal ``yyyy-mm-dd``.

4. Jika Anda memilih ``page``, Anda perlu mengisi parameter **limit** dengan angka sesuai dengan data yang ingin ditampilkan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-financials.png
  :width: 600
  :align: center

5. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial NCL Drawdown. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": [
            {
                "id": 3,
                "company": {
                    "id": 53,
                    "name": "PT Limasland Realty Cilegon"
                },
                "ncl_id": 2,
                "ncl_name": "sdfhjh",
                "project_id": 635,
                "project_name": "PT Limasland Realty Cilegon",
                "category_id": 1,
                "category_name": "ACTUAL",
                "posting_date": "2021-12-26T00:00:00+07:00",
                "trading_partner_id": 1,
                "trading_partner_name": "PT PP (Persero) Tbk",
                "bank_id": 5,
                "bank_name": "PT BANK DANAMON INDONESIA Tbk",
                "debt_type_id": 3,
                "debt_type_name": "NCL",
                "plafon_value": 1000000,
                "credit_line_detail": "drtfjj",
                "credit_line_opening_date": "2021-11-26T00:00:00+07:00",
                "maturity_date": "2021-11-26T00:00:00+07:00",
                "transaction_type": "PEMBAYARAN",
                "debt_allocation_group_id": 2,
                "debt_allocation_group_name": "SHAREHOLDER LOANS",
                "result_allocation_name": "PP01 - PT PP (Persero) Tbk",
                "value": -1000000,
                "is_reverse": 1,
                "reference_id": "0al7ZvJECt",
                "created_at": "2021-11-26T09:05:52+07:00",
                "updated_at": "2021-11-26T09:05:52+07:00"
            },
            {
                "id": 2,
                "company": {
                    "id": 53,
                    "name": "PT Limasland Realty Cilegon"
                },
                "ncl_id": 2,
                "ncl_name": "sdfhjh",
                "project_id": 635,
                "project_name": "PT Limasland Realty Cilegon",
                "category_id": 1,
                "category_name": "ACTUAL",
                "posting_date": "2021-11-26T00:00:00+07:00",
                "trading_partner_id": 1,
                "trading_partner_name": "PT PP (Persero) Tbk",
                "bank_id": 5,
                "bank_name": "PT BANK DANAMON INDONESIA Tbk",
                "debt_type_id": 3,
                "debt_type_name": "NCL",
                "plafon_value": 1000000,
                "credit_line_detail": "drtfjj",
                "credit_line_opening_date": "2021-11-26T00:00:00+07:00",
                "maturity_date": "2021-11-26T00:00:00+07:00",
                "transaction_type": "PENCAIRAN",
                "debt_allocation_group_id": 2,
                "debt_allocation_group_name": "SHAREHOLDER LOANS",
                "result_allocation_name": "PP01 - PT PP (Persero) Tbk",
                "value": 1000000,
                "is_reverse": 0,
                "reference_id": "0al7ZvJECt",
                "created_at": "2021-11-26T08:49:21+07:00",
                "updated_at": "2021-11-26T08:49:21+07:00"
            }
        ]
    }

Post
^^^^

1. Buka endpoint Post di API Financial NCL Drawdown, lalu klik tombol **Try it out** yang berada di pojok kanan atas.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial NCL Drawdown yang ingin disimpan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "company_id": 1,
        "ncl_id": 10,
        "posting_date": "2021-12-10",
        "trading_partner_id": 1,
        "bank_id": 1,
        "ncl_drawdown_data": [
            {
                "credit_line_detail": "TEST NCL DRAWDOWN 001",
                "credit_loan_opening_date": "2021-12-10",
                "maturity_date": "2021-12-10",
                "transaction_type": "PENCAIRAN",
                "debt_allocation_group_id": 1,
                "result_allocation_group_id": 100,
                "value": 700000000
            }
        ]
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

Get By Id
^^^^^^^^^

1. Buka endpoint Get By Id di API Financial NCL Drawdown, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial NCL Drawdown yang ingin ditampilkan.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-by-id-ncl-drawdown.png
  :width: 600
  :align: center

3. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial NCL Drawdown. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": {
            "id": 3,
            "company": {
                "id": 53,
                "name": "PT Limasland Realty Cilegon"
            },
            "ncl_id": 2,
            "ncl_name": "sdfhjh",
            "project_id": 635,
            "project_name": "PT Limasland Realty Cilegon",
            "category_id": 1,
            "category_name": "ACTUAL",
            "posting_date": "2021-12-26T00:00:00+07:00",
            "trading_partner_id": 1,
            "trading_partner_name": "PT PP (Persero) Tbk",
            "bank_id": 5,
            "bank_name": "PT BANK DANAMON INDONESIA Tbk",
            "debt_type_id": 3,
            "debt_type_name": "NCL",
            "plafon_value": 1000000,
            "credit_line_detail": "drtfjj",
            "credit_line_opening_date": "2021-11-26T00:00:00+07:00",
            "maturity_date": "2021-11-26T00:00:00+07:00",
            "transaction_type": "PEMBAYARAN",
            "debt_allocation_group_id": 2,
            "debt_allocation_group_name": "SHAREHOLDER LOANS",
            "result_allocation_name": "PP01 - PT PP (Persero) Tbk",
            "value": -1000000,
            "is_reverse": 1,
            "reference_id": "0al7ZvJECt",
            "created_at": "2021-11-26T09:05:52+07:00",
            "updated_at": "2021-11-26T09:05:52+07:00"
        }
    }

Put
^^^

1. Buka endpoint Put di API Financial NCL Drawdown, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial NCL Drawdown yang ingin diubah datanya.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial NCL Drawdown yang ingin diubah. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "value": 20000000
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

API Financial Reverse NCL Drawdown
------------------------------------------

Bagian ini menjelaskan bagaimana cara menjalankan API Financial Reverse NCL Drawdown melalui Swagger. Endpoint yang tersedia di API Financial Reverse NCL Drawdown hanya ada satu, yakni **Postt**.

Post
^^^^

1. Buka endpoint Post di API Financial Reverse NCL Drawdown, lalu klik tombol **Try it out** yang berada di pojok kanan atas.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial NCL Drawdown yang ingin disimpan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "company_id": 1,
        "financial_ncl_drawdown_id": 20
    }


API Financial Acceptation
-------------------------

Bagian ini menjelaskan bagaimana cara menjalankan API Financial Acceptation melalui Swagger. Endpoint yang tersedia di API Financial Acceptation ada empat, yakni **Get**, **Post**, **Get By Id**, dan **Put**.

Get
^^^

1. Buka endpoint Get di API Financial Acceptation, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

2. Isi parameter **display** dengan ``all`` untuk menampilkan semua data, atau ``page`` untuk menampilkan beberapa data.

3. Jika ingin menampilkan data dalam periode waktu tertentu, Anda juga dapat mengisi parameter **start-period** dan **finish-period** dengan format tanggal ``yyyy-mm-dd``.

4. Jika Anda memilih ``page``, Anda perlu mengisi parameter **limit** dengan angka sesuai dengan data yang ingin ditampilkan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-financials.png
  :width: 600
  :align: center

5. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial Acceptation. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": [
            {
                "id": 2,
                "company": {
                    "id": 53,
                    "name": "PT Limasland Realty Cilegon"
                },
                "ncl_drawdown_id": 2,
                "ncl_drawdown_name": "drtfjj",
                "project_id": 635,
                "project_name": "PT Limasland Realty Cilegon",
                "category_id": 1,
                "category_name": "ACTUAL",
                "posting_date": "2021-11-26T00:00:00+07:00",
                "maturity_date": "",
                "trading_partner_id": 2,
                "trading_partner_name": "PP Presisi",
                "bank_id": 1,
                "bank_name": "PT BANK RAKYAT INDONESIA (PERSERO) Tbk",
                "debt_type_id": 3,
                "debt_type_name": "NCL",
                "debt_allocation_group_id": 3,
                "debt_allocation_group_name": "SUBSIDIARY INVESTMENT",
                "result_allocation_name": "PP05 - PP Energi",
                "value": 1000000,
                "reference_id": "YMsgzmrjkM",
                "created_at": "2021-11-26T08:50:15+07:00",
                "updated_at": "2021-11-26T08:50:15+07:00"
            },
            {
                "id": 1,
                "company": {
                    "id": 27,
                    "name": "PP Infrastruktur"
                },
                "ncl_drawdown_id": 1,
                "ncl_drawdown_name": "Test NCL Draw 1",
                "project_id": 680,
                "project_name": "PT TIRTA TANGSEL MANDIRI",
                "category_id": 1,
                "category_name": "ACTUAL",
                "posting_date": "2021-11-26T00:00:00+07:00",
                "maturity_date": "",
                "trading_partner_id": 2,
                "trading_partner_name": "PP Presisi",
                "bank_id": 1,
                "bank_name": "PT BANK RAKYAT INDONESIA (PERSERO) Tbk",
                "debt_type_id": 3,
                "debt_type_name": "NCL",
                "debt_allocation_group_id": 3,
                "debt_allocation_group_name": "SUBSIDIARY INVESTMENT",
                "result_allocation_name": "PP05 - PP Energi",
                "value": 2900000,
                "reference_id": "sA2JLiXyOK",
                "created_at": "2021-11-26T08:43:14+07:00",
                "updated_at": "2021-11-26T08:43:14+07:00"
            }
        ]
    }

Post
^^^^

1. Buka endpoint Post di API Financial Acceptation, lalu klik tombol **Try it out** yang berada di pojok kanan atas.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial Acceptation yang ingin disimpan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "company_id": 1,
        "project_id": 1,
        "category_id": 1,
        "posting_date": "2021-12-10",
        "maturity_date": "2021-12-1-",
        "trading_partner_id": 1,
        "financial_ncl_id": 3,
        "financial_ncl_drawdown_id": 2,
        "debt_allocation_group": [
            {
                "debt_allocation_group_id": 1,
                "result_allocation_group_id": 100,
                "value": 300000000
            }
        ]
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

Get By Id
^^^^^^^^^

1. Buka endpoint Get By Id di API Financial Acceptation, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial Acceptation yang ingin ditampilkan.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-by-id-acceptation.png
  :width: 600
  :align: center

3. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial Acceptation. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": {
            "id": 2,
            "company": {
                "id": 53,
                "name": "PT Limasland Realty Cilegon"
            },
            "ncl_drawdown_id": 2,
            "ncl_drawdown_name": "drtfjj",
            "project_id": 635,
            "project_name": "PT Limasland Realty Cilegon",
            "category_id": 1,
            "category_name": "ACTUAL",
            "posting_date": "2021-11-26T00:00:00+07:00",
            "maturity_date": "",
            "trading_partner_id": 2,
            "trading_partner_name": "PP Presisi",
            "bank_id": 1,
            "bank_name": "PT BANK RAKYAT INDONESIA (PERSERO) Tbk",
            "debt_type_id": 3,
            "debt_type_name": "NCL",
            "debt_allocation_group_id": 3,
            "debt_allocation_group_name": "SUBSIDIARY INVESTMENT",
            "result_allocation_name": "PP05 - PP Energi",
            "value": 1000000,
            "reference_id": "YMsgzmrjkM",
            "created_at": "2021-11-26T08:50:15+07:00",
            "updated_at": "2021-11-26T08:50:15+07:00"
        }
    }

Put
^^^

1. Buka endpoint Put di API Financial Acceptation, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial Acceptation yang ingin diubah datanya.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial Acceptation yang ingin diubah. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "value": 20000000
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

API Financial Cash Conversion Cycle
-----------------------------------

Bagian ini menjelaskan bagaimana cara menjalankan API Financial Cash Conversion Cycle melalui Swagger. Endpoint yang tersedia di API Financial Cash Conversion Cycle ada empat, yakni **Get**, **Post**, **Get By Id**, dan **Put**.

Get
^^^

1. Buka endpoint Get di API Financial Cash Conversion Cycle, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

2. Isi parameter **display** dengan ``all`` untuk menampilkan semua data, atau ``page`` untuk menampilkan beberapa data.

3. Jika ingin menampilkan data dalam periode waktu tertentu, Anda juga dapat mengisi parameter **start-period** dan **finish-period** dengan format tanggal ``yyyy-mm-dd``.

4. Jika Anda memilih ``page``, Anda perlu mengisi parameter **limit** dengan angka sesuai dengan data yang ingin ditampilkan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-financials.png
  :width: 600
  :align: center

5. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial Cash Conversion Cycle. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": [
            {
                "id": 7,
                "company": {
                    "id": 1,
                    "name": "PP Konsolidasi"
                },
                "project_id": 1,
                "project_name": "421501 BENDUNGAN LOLAK - SULAWESI UTARA",
                "posting_date": "2021-11-29T00:00:00+07:00",
                "trading_partner": "PT PP (Persero) Tbk",
                "category_id": 1,
                "category_name": "ACTUAL",
                "flag_ccc_id": 1,
                "flag_ccc_name": "PIUTANG USAHA",
                "value": 99999,
                "reference_id": "hpsg8DGphp",
                "created_at": "2021-11-29T13:46:50+07:00",
                "updated_at": "2021-11-29T14:01:23+07:00"
            },
            {
                "id": 8,
                "company": {
                    "id": 1,
                    "name": "PP Konsolidasi"
                },
                "project_id": 1,
                "project_name": "421501 BENDUNGAN LOLAK - SULAWESI UTARA",
                "posting_date": "2021-11-29T00:00:00+07:00",
                "trading_partner": "PT PP (Persero) Tbk",
                "category_id": 1,
                "category_name": "ACTUAL",
                "flag_ccc_id": 2,
                "flag_ccc_name": "HUTANG USAHA",
                "value": 0,
                "reference_id": "hpsg8DGphp",
                "created_at": "2021-11-29T13:46:50+07:00",
                "updated_at": "2021-11-29T13:46:50+07:00"
            }
        ]
    }

Post
^^^^

1. Buka endpoint Post di API Financial Cash Conversion Cycle, lalu klik tombol **Try it out** yang berada di pojok kanan atas.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial Cash Conversion Cycle yang ingin disimpan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "company_id": 1,
        "project_id": 1,
        "category_id": 1,
        "posting_date": "2021-12-10",
        "trading_partner_id": 1,
        "data_cash_conversion_cycle": [
            {
                "id": 1,
                "value": 100000
            },
            {
                "id": 2,
                "value": 100000
            },
            {
                "id": 3,
                "value": 100000
            }
        ]
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

Get By Id
^^^^^^^^^

1. Buka endpoint Get By Id di API Financial Cash Conversion Cycle, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial Cash Conversion Cycle yang ingin ditampilkan.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-by-id-cash-conversion-cycle.png
  :width: 600
  :align: center

3. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial Cash Conversion Cycle. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": {
            "id": 8,
            "company": {
                "id": 1,
                "name": "PP Konsolidasi"
            },
            "project_id": 1,
            "project_name": "421501 BENDUNGAN LOLAK - SULAWESI UTARA",
            "posting_date": "2021-11-29T00:00:00+07:00",
            "trading_partner": "PT PP (Persero) Tbk",
            "category_id": 1,
            "category_name": "ACTUAL",
            "flag_ccc_id": 2,
            "flag_ccc_name": "HUTANG USAHA",
            "value": 0,
            "reference_id": "hpsg8DGphp",
            "created_at": "2021-11-29T13:46:50+07:00",
            "updated_at": "2021-11-29T13:46:50+07:00"
        }
    }

Put
^^^

1. Buka endpoint Put di API Financial Cash Conversion Cycle, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial Cash Conversion Cycle yang ingin diubah datanya.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial Cash Conversion Cycle yang ingin diubah. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "value": 20000000
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

API Financial Project Profitability
-----------------------------------

Bagian ini menjelaskan bagaimana cara menjalankan API Financial Project Profitability melalui Swagger. Endpoint yang tersedia di API Financial Project Profitability ada empat, yakni **Get**, **Post**, **Get By Id**, dan **Put**.

Get
^^^

1. Buka endpoint Get di API Financial Project Profitability, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

2. Isi parameter **display** dengan ``all`` untuk menampilkan semua data, atau ``page`` untuk menampilkan beberapa data.

3. Jika ingin menampilkan data dalam periode waktu tertentu, Anda juga dapat mengisi parameter **start-period** dan **finish-period** dengan format tanggal ``yyyy-mm-dd``.

4. Jika Anda memilih ``page``, Anda perlu mengisi parameter **limit** dengan angka sesuai dengan data yang ingin ditampilkan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-financials.png
  :width: 600
  :align: center

5. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial Project Profitability. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": [
            {
                "id": 33,
                "company": {
                    "id": 27,
                    "name": "PP Infrastruktur"
                },
                "project_id": 679,
                "project_name": "PT WIDYA TIRTA SELARAS",
                "category_id": 2,
                "category_name": "PLAN",
                "posting_date": "2021-11-29T00:00:00+07:00",
                "trading_partner": "PT PP (Persero) Tbk",
                "flag_profitability_id": 1,
                "flag_profitability_name": "REVENUE",
                "value": 999,
                "reference_id": "cF4PDsJ6uo",
                "created_at": "2021-11-29T13:08:18+07:00",
                "updated_at": "2021-11-29T13:25:07+07:00"
            },
            {
                "id": 34,
                "company": {
                    "id": 27,
                    "name": "PP Infrastruktur"
                },
                "project_id": 679,
                "project_name": "PT WIDYA TIRTA SELARAS",
                "category_id": 2,
                "category_name": "PLAN",
                "posting_date": "2021-11-29T00:00:00+07:00",
                "trading_partner": "PT PP (Persero) Tbk",
                "flag_profitability_id": 2,
                "flag_profitability_name": "PRODUCTION COST",
                "value": 0,
                "reference_id": "cF4PDsJ6uo",
                "created_at": "2021-11-29T13:08:18+07:00",
                "updated_at": "2021-11-29T13:08:18+07:00"
            }
        ]
    }

Post
^^^^

1. Buka endpoint Post di API Financial Project Profitability, lalu klik tombol **Try it out** yang berada di pojok kanan atas.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial Project Profitability yang ingin disimpan. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "company_id": 1,
        "project_id": 1,
        "posting_date": "2021-12-10",
        "trading_partner_id": 1,
        "data_project_profitability_plan": [
            {
                "id": 1,
                "value": 100000
            },
            {
                "id": 2,
                "value": 100000
            },
            {
                "id": 3,
                "value": 100000
            },
            {
                "id": 4,
                "value": 100000
            },
            {
                "id": 5,
                "value": 100000
            },
            {
                "id": 6,
                "value": 100000
            },
            {
                "id": 7,
                "value": 100000
            },
            {
                "id": 8,
                "value": 100000
            }
        ],
        "data_project_profitability_actual": [
            {
                "id": 1,
                "value": 200000
            },
            {
                "id": 2,
                "value": 200000
            },
            {
                "id": 3,
                "value": 200000
            },
            {
                "id": 4,
                "value": 200000
            },
            {
                "id": 5,
                "value": 200000
            },
            {
                "id": 6,
                "value": 200000
            },
            {
                "id": 7,
                "value": 200000
            },
            {
                "id": 8,
                "value": 200000
            }
        ]
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }

Get By Id
^^^^^^^^^

1. Buka endpoint Get By Id di API Financial Project Profitability, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial Project Profitability yang ingin ditampilkan.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``. Contoh pengisian parameter:

.. image:: ../_static/swagger/get-by-id-project-profitability.png
  :width: 600
  :align: center

3. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi data Financial Project Profitability. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200,
        "data": {
            "id": 33,
            "company": {
                "id": 27,
                "name": "PP Infrastruktur"
            },
            "project_id": 679,
            "project_name": "PT WIDYA TIRTA SELARAS",
            "category_id": 2,
            "category_name": "PLAN",
            "posting_date": "2021-11-29T00:00:00+07:00",
            "trading_partner": "PT PP (Persero) Tbk",
            "flag_profitability_id": 1,
            "flag_profitability_name": "REVENUE",
            "value": 999,
            "reference_id": "cF4PDsJ6uo",
            "created_at": "2021-11-29T13:08:18+07:00",
            "updated_at": "2021-11-29T13:25:07+07:00"
        }
    }

Put
^^^

1. Buka endpoint Put di API Financial Project Profitability, lalu klik tombol **Try it out** yang berada di pojok kanan atas. Isi parameter **id** dengan id data Financial Project Profitability yang ingin diubah datanya.

2. Isi parameter **Authorization** dengan token yang tadi sudah didapatkan saat proses login dengan format ``Bearer <nilai token>``.

3. Isi payload di **body** dengan data Financial Project Profitability yang ingin diubah. Setelah itu klik tombol **Execute** untuk mengeksekusi API. Contoh payload:

.. code-block:: JSON

    {
        "value": 20000000
    }

4. Jika berhasil, maka di bagian bawah akan muncul ``response`` yang berisi keterangan ``status``, ``message``, dan ``code``. Contoh hasil ``response`` sukses:

.. code-block:: JSON

    {
        "status": true,
        "message": "success",
        "code": 200
    }